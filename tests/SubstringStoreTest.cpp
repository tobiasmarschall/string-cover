#include <boost/test/unit_test.hpp>
#include "../src/SubstringStore.h"

using namespace std;

BOOST_AUTO_TEST_CASE( SubstringStoreTest )
{
	vector<string> v;
	v.push_back("ABC");
	v.push_back("BCD");
	SubstringStore s(&v);
	BOOST_CHECK_EQUAL(9, s.size());
	BOOST_CHECK_EQUAL("BC", s.getSubstring(s.getIndex(0,1,3)));
	BOOST_CHECK_EQUAL("BC", s.getSubstring(s.getIndex(1,0,2)));
	BOOST_CHECK_EQUAL("D", s.getSubstring(s.getIndex(1,2,3)));
	BOOST_CHECK_EQUAL(1, s.getCount(s.getIndex(0,0,1)));
	BOOST_CHECK_EQUAL(2, s.getCount(s.getIndex(1,0,2)));
	BOOST_CHECK_EQUAL(2, s.getCount(s.getIndex(0,1,3)));
	vector<string> v2;
	v2.push_back("ACGTATGTGGGGGTCTCGGATAGCAGCGAGTCGTAACGCG");
	v2.push_back("TGGTGCGATGGTAGTAGGTGCGCGACCATCAGC");
	v2.push_back("CGCATGCCGTGCGTCGTAATGTCCTAGGACTCGAGGGC");
	SubstringStore s2(&v2);
	for (size_t k=0; k<v2.size(); ++k) {
		const string& str = v2[k];
		for (size_t i=0; i<str.size(); ++i) {
			for (size_t j=i+1; j<=str.size(); ++j) {
				BOOST_CHECK_EQUAL(str.substr(i, j-i), s2.getSubstring(s2.getIndex(k,i,j)));
			}
		}
	}
}
