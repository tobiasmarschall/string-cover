/* Copyright 2011 Tobias Marschall, Email: tm@cwi.nl 
 * 
 * This file is part of string-cover.
 *
 * string-cover is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * string-cover is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with string-cover.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIMPLEBOUNDPROVIDER_H_
#define SIMPLEBOUNDPROVIDER_H_

#include "BranchAndBoundNode.h"
#include "BoundProvider.h"
#include "Multipliers.h"

/** Computes bounds based on shortest paths without adapting the Lagrangian mulipliers. */
class SimpleBoundProvider: public BoundProvider {
private:
	const Multipliers& multipliers;
	int min_length;
	int max_length;
public:
	/** Assumes that the given multipliers are such that the multipliers belonging
	 *  to the same substring sum up to the substrings weight.
	 *  WARNING: If this condition is violated this class might yield wrong bounds.
	 */
	SimpleBoundProvider(const Multipliers& multipliers);
	virtual ~SimpleBoundProvider();

	/** Solves a shortest path problem on the index graphs for fixed multipliers and returns
	 *  a lower bound for the optimal solution and a (possibly suboptimal) solution.
	 *  Respects included/forbidden strings as given by branch-and-bound node.
	 */
	virtual std::auto_ptr<Solution> compute_bounds(BranchAndBoundNode& node, double global_upper_bound, double* lower_bound, double* substring_weight_sums) const;
	virtual void setMinLength(int min_length);
	virtual void setMaxLength(int max_length);
};

#endif /* SIMPLEBOUNDPROVIDER_H_ */
