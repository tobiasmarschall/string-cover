/* Copyright 2011 Tobias Marschall, Email: tm@cwi.nl 
 * 
 * This file is part of string-cover.
 *
 * string-cover is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * string-cover is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with string-cover.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SOLUTION_H_
#define SOLUTION_H_

#include <vector>
#include <iostream>
#include <boost/dynamic_bitset.hpp>

#include "StringFactorization.h"
#include "ProblemInstance.h"

class Solution {
private:
	const ProblemInstance& instance;
	std::auto_ptr<std::vector<StringFactorization> > factorizations;
	boost::dynamic_bitset<> solution_substrings;
	double score;
	void computSolutionSubset();
public:
	/** Constructor that computes the solution subset and the score from the given
	 *  factorization. */
	Solution(const ProblemInstance& instance, std::auto_ptr<std::vector<StringFactorization> >  factorizations);
	/** Constructor that computes the solution subset from the given factorization. */
	Solution(const ProblemInstance& instance, double score, std::auto_ptr<std::vector<StringFactorization> >  factorizations);
	explicit Solution(const Solution& solution);
	virtual ~Solution();
	virtual double getScore() const;
	/** Returns the substrings contained in the solution. */
	virtual const boost::dynamic_bitset<>& getSubstrings() const;
	virtual void printFactorizations(std::ostream& os) const;
	static std::auto_ptr<Solution> createAllStringsSolution(const ProblemInstance& instance);
	static std::auto_ptr<Solution> createAllLettersSolution(const ProblemInstance& instance);
    friend std::ostream& operator<<(std::ostream& os, const Solution& solution);
};

#endif /* SOLUTION_H_ */
