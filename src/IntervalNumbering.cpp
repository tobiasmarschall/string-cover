/* Copyright 2011 Tobias Marschall, Email: tm@cwi.nl 
 * 
 * This file is part of string-cover.
 *
 * string-cover is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * string-cover is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with string-cover.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "IntervalNumbering.h"

#include <cassert>

using namespace std;

IntervalNumbering::IntervalNumbering(const vector<string>* strings) : strings(strings) {
	offsets = new size_t[strings->size()];
	size_t n = 0;
	for (size_t i=0; i<strings->size(); ++i) {
		offsets[i] = n;
		size_t m = strings->at(i).size();
		n += m*(m+1)/2;
	}
	total_intervals = n;
}

IntervalNumbering::~IntervalNumbering() {
	delete[] offsets;
}

size_t IntervalNumbering::getIndex(size_t string_nr, size_t start_pos, size_t end_pos) const {
	assert(string_nr < strings->size());
	assert(start_pos < end_pos);
	assert(end_pos <= strings->at(string_nr).size());
	size_t l = end_pos - start_pos;
	size_t m = strings->at(string_nr).size() - start_pos - 1;
	return offsets[string_nr] + m*(m+1)/2 + l - 1;
}



size_t IntervalNumbering::size() const {
	return total_intervals;
}



