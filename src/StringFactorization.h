/* Copyright 2011 Tobias Marschall, Email: tm@cwi.nl 
 * 
 * This file is part of string-cover.
 *
 * string-cover is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * string-cover is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with string-cover.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STRINGFACTORIZATION_H_
#define STRINGFACTORIZATION_H_

#include <boost/dynamic_bitset.hpp>
#include <boost/function_output_iterator.hpp>

class StringFactorization;

namespace boost {
	/** Function allowing to use boost::dynamic_bitset as key in hash sets and maps. */
	std::size_t hash_value(const StringFactorization& st);
}

/** A factorization of a single string. That means, this class stores a set of
 *  fragment borders (=breakpoints). */
class StringFactorization {
private:
	boost::dynamic_bitset<> bitarray;
public:
	typedef std::pair<size_t,size_t> interval_t;

	/** Constructs a factorizition for a given string length. */
	StringFactorization(size_t string_length);

	StringFactorization(const boost::dynamic_bitset<>& bitarray);

	/** Constructs a factorizition for a given string length.
	 *  @param start_value The factorization given as a long that is unterpreted as bitarray.*/
	StringFactorization(size_t string_length, unsigned long start_value);

	virtual ~StringFactorization();

	/** Modifies the factorization by splitting the text after the given position. */
	void addBreakpoint(size_t position);

	/** Splits the text at all positions. */
	void addAllBreakpoints();

	/** Returns a bitarray of breakpoints; if bit at position i is set, then
	 *  a factor boundary (breakpoint) is between character i and i+1. */
	const boost::dynamic_bitset<>& getBreakpoints() const;

	std::string printFactorizedString(std::string s) const;

	/** Returns the intervals as pairs: start (inclusive) and end (exclusive). */
	std::auto_ptr<std::vector<interval_t> > getIntervals() const;

	/** Returns the length of the strings this factorization applies to. */
	size_t size() const;

	bool operator==(const StringFactorization& st) const;

	friend std::size_t boost::hash_value(const StringFactorization& st);
};


#endif /* STRINGFACTORIZATION_H_ */
