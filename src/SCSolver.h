/* Copyright 2011 Tobias Marschall, Email: tm@cwi.nl 
 * 
 * This file is part of string-cover.
 *
 * string-cover is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * string-cover is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with string-cover.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SCSOLVER_H_
#define SCSOLVER_H_

#include <vector>

#include "ProblemInstance.h"
#include "Solution.h"

/** Abstract base class for solvers of the String Cover Problem. */
class SCSolver {
public:
	virtual std::auto_ptr<Solution> solve(const ProblemInstance& instance) = 0;
	virtual ~SCSolver();
};

#endif /* SCSOLVER_H_ */
