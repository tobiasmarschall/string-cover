/* Copyright 2011 Tobias Marschall, Email: tm@cwi.nl 
 * 
 * This file is part of string-cover.
 *
 * string-cover is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * string-cover is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with string-cover.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PROBLEMINSTANCE_H_
#define PROBLEMINSTANCE_H_

#include <string>
#include <vector>
#include <boost/dynamic_bitset.hpp>

#include "SubstringStore.h"
#include "IntervalNumbering.h"
#include "WeightFunction.h"

class ProblemInstance {
private:
	std::vector<std::string>* strings;
	SubstringStore* substrings;
	IntervalNumbering* interval_numbering;
	double* weight_table;
	bool integer_weights;
public:
	/** Constructor. */
	ProblemInstance(std::auto_ptr<std::vector<std::string> > strings);
	virtual ~ProblemInstance();

	const std::vector<std::string>& getStrings() const;
	const SubstringStore& getSubstrings() const;

	double getWeight(size_t substring_idx) const;

	const IntervalNumbering& getIntervalNumbering() const;
	bool hasIntegerWeights() const;

	void cacheWeights(const WeightFunction& w);
};

#endif /* PROBLEMINSTANCE_H_ */
