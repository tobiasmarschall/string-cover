/* Copyright 2011 Tobias Marschall, Email: tm@cwi.nl 
 * 
 * This file is part of string-cover.
 *
 * string-cover is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * string-cover is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with string-cover.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BRANCHANDBOUNDSOLVER_H_
#define BRANCHANDBOUNDSOLVER_H_

#include <queue>
#include "BranchAndBoundNode.h"
#include "BranchingRule.h"

#include "SCSolver.h"

class BranchAndBoundSolver: public SCSolver {
public:
	struct Comparator {
	     bool operator()(const BranchAndBoundNode* node1, const BranchAndBoundNode* node2) {
	    	 return node1->getPriority() > node2->getPriority();
	     }
	};
	typedef std::priority_queue<BranchAndBoundNode*,std::vector<BranchAndBoundNode*>,Comparator> node_queue_t;
private:
	BoundProvider& bound_provider;
	const BranchingRule<node_queue_t>& branching_rule;
	int status_iterations;
	int min_length;
	int max_length;
public:
	BranchAndBoundSolver(BoundProvider& bound_provider, const BranchingRule<node_queue_t>& branching_rule);
	virtual ~BranchAndBoundSolver();

	virtual std::auto_ptr<Solution> solve(const ProblemInstance& instance);

	/** Sets the number of iterations after which a status message is printed. */
	virtual void setStatusIterations(int iterations);

	/** Set minimal length of allowed substrings. Set to -1 to desable. */
	virtual void setMinLength(int min_length);
	/** Set maximal length of allowed substrings. Set to -1 to desable. */
	virtual void setMaxLength(int max_length);
};

#endif /* BRANCHANDBOUNDSOLVER_H_ */
