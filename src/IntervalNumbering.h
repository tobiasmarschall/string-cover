/* Copyright 2011 Tobias Marschall, Email: tm@cwi.nl 
 * 
 * This file is part of string-cover.
 *
 * string-cover is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * string-cover is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with string-cover.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INTERVALNUMBERING_H_
#define INTERVALNUMBERING_H_

#include <vector>
#include <string>

/** Provides a numbering of all intervals in a string set. */
class IntervalNumbering {
private:
	const std::vector<std::string>* strings;
	size_t* offsets;
	size_t total_intervals;
public:
	IntervalNumbering(const std::vector<std::string>* strings);
	virtual ~IntervalNumbering();

	/** Returns the unique index of the interval from start_pos (inclusively) to
	 *  end_pos (exclusively) in string string_nr. */
	virtual size_t getIndex(size_t string_nr, size_t start_pos, size_t end_pos) const;
	/** Return the number of intervals. */
	virtual size_t size() const;
};

#endif /* INTERVALNUMBERING_H_ */
