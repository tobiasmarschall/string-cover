/* Copyright 2011 Tobias Marschall, Email: tm@cwi.nl 
 * 
 * This file is part of string-cover.
 *
 * string-cover is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * string-cover is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with string-cover.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ProblemInstance.h"

using namespace std;

ProblemInstance::ProblemInstance(auto_ptr<vector<string> > strings) {
	assert(strings.get()!=0);
	this->strings = strings.release();
	this->substrings = new SubstringStore(this->strings);
	this->interval_numbering = new IntervalNumbering(this->strings);
	this->weight_table = 0;
	this->integer_weights = true;
}

ProblemInstance::~ProblemInstance() {
	delete interval_numbering;
	delete substrings;
	delete strings;
	if (weight_table!=0) delete weight_table;
}

const std::vector<std::string>& ProblemInstance::getStrings() const {
	return *strings;
}

const SubstringStore& ProblemInstance::getSubstrings() const {
	return *substrings;
}

double ProblemInstance::getWeight(size_t substring_idx) const {
	if (weight_table == 0) return 1.0;
	else return weight_table[substring_idx];
}

bool ProblemInstance::hasIntegerWeights() const {
	return integer_weights;
}

const IntervalNumbering & ProblemInstance::getIntervalNumbering() const {
	return *interval_numbering;
}

void ProblemInstance::cacheWeights(const WeightFunction& w) {
	if (weight_table == 0) {
		weight_table = new double[substrings->size()];
	}
	for (size_t i=0; i<substrings->size(); ++i) {
		weight_table[i] = w.getWeight(substrings->getSubstring(i));
	}
	integer_weights = w.isInteger();
}










